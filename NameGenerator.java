import java.util.*;

public class NameGenerator {
    Random rand;
    public NameGenerator() {
	    rand = new Random();
    }

    public boolean randChance(int x) {
        return (rand.nextInt(1, 100) <= x);
    }

    public String generateName(int len) {
        final String vowels = "aaaeeeiiiooouuuyy";
        final String frictive = "rsfhvnmz";
        final String plosive = "tpdgkbc";
    	final String weird = "qwjx";

        String name = "";

        int syllables = 0;
        char state;
        boolean prime = false;

        if (randChance(30)) {
            state = 'v';
        }
        else if (randChance(40)) {
            state = 'f';
        }
		else if (randChance(70)) {
			state = 'p';
		}
		else {
			state = 'w';
		}

		for (int i = 0; i < len; i++) {
			switch (state) {
				case 'v':
					name += vowels.charAt(rand.nextInt(vowels.length()));
					if (!prime)
						syllables++;
					break;
				case 'f':
					name += frictive.charAt(rand.nextInt(frictive.length()));
					break;
				case 'p':
					name += plosive.charAt(rand.nextInt(plosive.length()));
					break;
				case 'w':
					name += weird.charAt(rand.nextInt(weird.length()));
					break;
			}

			if (syllables > 0 && i >= 3) {
				if (randChance(20+i*4)) {
					break;
				}
			}

			switch (state) {
				case 'v':
					if (!prime && randChance(10)) {
						state = 'v';
						prime = true;
						break;
					}
					else if (randChance(40)) {
						state = 'f';
					}
					else if (randChance(70)) {
						state = 'p';
					}
					else {
						state = 'w';
					}
					prime = false;
					break;
				case 'f':
					if (!prime && randChance(50)) {
						prime = true;
						state = 'p';
						break;
					}
					state = 'v';
					prime = false;
					break;
				case 'p':
					if (!prime && randChance(10)) {
						prime = true;
						state = 'f';
						break;
					}
					state = 'v';
					prime = false;
					break;
				case 'w':
					state = 'v';
					prime = false;
					break;
			}
		}
		name = name.substring(0, 1).toUpperCase() + name.substring(2);
		return name;
	}

	public static void main(String[] args) {
		NameGenerator nameGenerator = new NameGenerator();
		System.out.println(nameGenerator.generateName(10));
	}
}
