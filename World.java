import java.util.*;
import java.awt.Point;

enum PlanetType {LAVA, ROCK, DESERT, FOREST, ICE;
   public static PlanetType randomType() {
      PlanetType[] planetTypes = PlanetType.values();      
      Random rand = new Random();
      return planetTypes[rand.nextInt(planetTypes.length)];
   }

}

class World {
   int width;
   int height;
   char[][] tiles;
   PlanetType type;
   NameGenerator nameGenerator;
   String name;
   Random rand;
   HashMap<Point, Town> towns;

   public World(int width, int height, PlanetType type) {
      this.width = width;
      this.height = height;
      rand = new Random();
      tiles = new char[height][width];
      towns = new HashMap<Point, Town>();
      nameGenerator = new NameGenerator();
      name = nameGenerator.generateName(10);
      this.type = type;

      switch (type) {
         case LAVA:
            for (int i = 0; i < height; i++) {
               for (int j = 0; j < width; j++) {
                  tiles[i][j] = 'M';
               }
            }
            break;
         case FOREST:
            for (int i = 0; i < height; i++) {
               for (int j = 0; j < width; j++) {
                  if (i == 0 || i == height - 1) {
                     tiles[i][j] = '*';
                  }
                  else {
                     tiles[i][j] = '~';
                  }
               }
            }
            break;
         case ROCK:
         case DESERT:
            for (int i = 0; i < height; i++) {
               for (int j = 0; j < width; j++) {
                  tiles[i][j] = '.';
               }
            }
            break;
         case ICE:
            for (int i = 0; i < height; i++) {
               for (int j = 0; j < width; j++) {
                  tiles[i][j] = '*';
               }
            }
            break;
      }
   }

   public void markLand(int x, int y, int p) {
      if (x < 0 || y < 1 || x >= width || y >= height - 1) {
         return;
      }
      if (tiles[y][x] == '~' || tiles[y][x] == 'M' || tiles[y][x] == '*') {
         tiles[y][x] = '.';
         if (p > 0) {
            if (rand.nextInt(0, 2) == 1) {
               markLand(x - 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markLand(x + 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markLand(x, y - 1, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markLand(x, y + 1, p - 1);
            }
         }
      }
   }

   public void markWater(int x, int y, int p) {
      if (x < 0 || y < 1 || x >= width || y >= height - 1) {
         return;
      }
      if (tiles[y][x] == '.') {
         tiles[y][x] = '~';
         if (p > 0) {
            if (rand.nextInt(0, 2) == 1) {
               markWater(x - 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markWater(x + 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markWater(x, y - 1, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markWater(x, y + 1, p - 1);
            }
         }
      }
   }

   public void markMountain(int x, int y, int p) {
      if (x < 0 || y < 1 || x >= width || y >= height - 1) {
         return;
      }
      if (tiles[y][x] == '.') {
         tiles[y][x] = '#';
         if (p > 0) {
            if (rand.nextInt(0, 2) == 1) {
               markMountain(x - 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markMountain(x + 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markMountain(x, y - 1, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markMountain(x, y + 1, p - 1);
            }
         }
      }
   }

   public void markForest(int x, int y, int p) {
      if (x < 0 || y < 1 || x >= width || y >= height - 1) {
         return;
      }
      if (tiles[y][x] == '.') {
         tiles[y][x] = '^';
         if (p > 0) {
            if (rand.nextInt(0, 2) == 1) {
               markForest(x - 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markForest(x + 1, y, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markForest(x, y - 1, p - 1);
            }
            if (rand.nextInt(0, 2) == 1) {
               markForest(x, y + 1, p - 1);
            }
         }
      }
   }

   public void markTown(int x, int y) {
      tiles[y][x] = 'X';
      Town town = new Town(64, 64);
      town.generate();
      towns.put(new Point(x, y), town);
   }

   public PlanetType getType() {
      return type;
   }

   public void generate() {

      if (type == PlanetType.FOREST || type == PlanetType.LAVA || type == PlanetType.ICE) {
         // Create the landmasses
         for (int i = 0; i < 5; i++) {
            int x = rand.nextInt(0, width);
            int y = rand.nextInt(1, height - 1);
            int p = rand.nextInt(5, 20);
            markLand(x, y, p);
         }
      }
      else if (type == PlanetType.DESERT) {
         // Create the small bodies of water
         for (int i = 0; i < 5; i++) {
            int x = rand.nextInt(0, width);
            int y = rand.nextInt(1, height - 1);
            int p = rand.nextInt(2, 4);
            markWater(x, y, p);
         }
      }

      // Create mountain ranges
      for (int i = 0; i < 7; i++) {
         while (true) {
            int x = rand.nextInt(0, width);
            int y = rand.nextInt(1, height - 1);
            int p = rand.nextInt(1, 3);
            if (tiles[y][x] == '.') {
               markMountain(x, y, p);
               break;
            }
         }
      }
      
      if (type == PlanetType.FOREST) {
         // Create forests
         for (int i = 0; i < 7; i++) {
            while (true) {
               int x = rand.nextInt(0, width);
               int y = rand.nextInt(1, height - 1);
               int p = rand.nextInt(1, 8);
               if (tiles[y][x] == '.') {
                  markForest(x, y, p);
                  break;
               }
            }
         }
      }

      if (type != PlanetType.LAVA && type != PlanetType.ROCK) {
         // Create towns
         int townNo = rand.nextInt(3, 10);
         for (int i = 0; i < townNo; i++) {
            while (true) {
               int x = rand.nextInt(0, width);
               int y = rand.nextInt(1, height - 1);
               if (tiles[y][x] == '.' || tiles[y][x] == '^') {
                  markTown(x, y);
                  break;
               }
            }
         }
      }
   }

   public void drawMap() {
      System.out.println(name);
      System.out.println("Planet type: " + type);
      for (int i = 0; i < height; i++) {
         for (int j = 0; j < width; j++) {
            System.out.print(tiles[i][j]);
         }
            System.out.println();
      }

      if (!towns.isEmpty()) {
         System.out.println("Towns: ");
         // Draw the towns
         for (Point p : towns.keySet()) {
            System.out.println("----------------------------------------------------------------");
            System.out.println("Location: " + p.x + " " + p.y);
            towns.get(p).drawMap();
         }
      }
   }

   public static void main(String[] args) {
      World world = new World(48, 48, PlanetType.FOREST);
      world.generate();
      world.drawMap();
   }
}
