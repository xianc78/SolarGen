import java.util.Random;
import java.util.ArrayList;

public class SolarSystem {
   ArrayList<World> worldList;
   Random rand;
   NameGenerator nameGenerator;
   String name;
   public SolarSystem() {
      worldList = new ArrayList<World>();
      ArrayList<PlanetType> planetTypes = new ArrayList<PlanetType>();
      rand = new Random();
      nameGenerator = new NameGenerator();
      name = nameGenerator.generateName(10);
      int planetCount = rand.nextInt(3, 5);
      for (int i = 0; i < planetCount; i++) {
         planetTypes.add(PlanetType.randomType());
      }

      for (int i = 0; i < planetCount; i++) {
         World world = new World(64, 64, planetTypes.get(i));
         world.generate();
         worldList.add(world);
      }
   }

   public void displaySystem() {
      for (World world : worldList) {
         System.out.println(name + " System");
         world.drawMap();
         System.out.println("\n========================================================================\n");
      }
   }

   public static void main(String[] args) {
      SolarSystem solarSystem = new SolarSystem();
      solarSystem.displaySystem();
   }
}
