import java.util.Random;

public class Town {
   int width;
   int height;
   char[][] tiles;
   Random rand;
   NameGenerator nameGenerator;
   String name;

   public Town(int width, int height) {
      this.width = width;
      this.height = height;
      rand = new Random();
      nameGenerator = new NameGenerator();
      name = nameGenerator.generateName(10);
      tiles = new char[height][width];
      for (int i = 0; i < height; i++) {
         for (int j = 0; j < width; j++) {
            tiles[i][j] = ' ';
         }
      }
   }

   public void generate() {
      int count = 0;
      //makeRoom(7, 7, 5, 5);
      for (int i = 0; i < 5; i++){
         while (true) {
            int x = rand.nextInt(0, width);
            int y = rand.nextInt(0, height);
            int w = rand.nextInt(3, 8) * 2;
            int h = rand.nextInt(3, 8) * 2;
            if (isSpace(x, y, w, h + 2)) {
               makeRoom(x, y, w, h);
               break;
            }
         }
      }
   }

   public boolean isSpace(int x, int y, int w, int h) {
      for (int i = y; i < y + h; i++) {
         for (int j = x; j < x + w; j++) {
            if (isOutOfBounds(j, i) || tiles[i][j] != ' ') {
               return false;
            }
         }
      }
      return true;
   }

   public boolean isOutOfBounds(int x, int y) {
      if (x < 0 || x >= width || y < 0 || y >= height) {
         return true;
      }
      return false;
   }

   public void makeRoom(int x, int y, int w, int h) {
      for (int i = y; i < y + h; i++) {
         for (int j = x; j < x + w; j++) {
            if (i == y || i == y + h - 1 || j == x || j == x + w - 1) {
               if (i == y + h - 1 && (j == x + (w / 2) || j == x + (w / 2) - 1)) {
                  tiles[i][j] = '.';
               }
               else {
                  tiles[i][j] = '#';
               }
            }
            else {
               tiles[i][j] = '.';
            }
         }
      }
   }

   public void drawMap() {
      System.out.println(name);
      for (int i = 0; i < height; i++) {
         for (int j = 0; j < width; j++) {
            System.out.print(tiles[i][j]);
         }
            System.out.println();
      }
   }

   public static void main(String[] args) {
      Town town = new Town(64, 64);
      town.generate();
      town.drawMap();
   }
}
