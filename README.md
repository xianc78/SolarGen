# Solar System Generator
A random solar system generator. Generates a series of planets with their own biome and even towns.  
This is merley just me practicing procedural generation.

## How to Compile
Just type
```
javac World.java
javac SolarSystem.java
```
into the terminal.

## How to run
Type 
```
java SolarSystem
```
and a random solar system will be generated for you.
